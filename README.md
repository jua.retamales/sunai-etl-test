# sunai-ETL-test
Prueba de Selección de Personal (PSP)

Documentacion API: http://3.144.48.134/docs#/

URL API: 3.144.48.134/api/

Requisitos:

- ​	Tener instalado Python en la versión 3.7 o superior
- ​	Tener una cuenta de Gitlab y (opcional) programa para subir resultados (ej Sublime Merge), en caso de no tener conocimientos de Git indicar con anticipación.
- ​	(opcional) IDE VSCode o equivalente

Pasos:

- [ ] Realiza fork a este repositorio
- [ ] Dale permisos a este usuario @jua.retamales
- [ ] Leer detenidamente [la prueba](https://gitlab.com/jua.retamales/sunai-etl-test/-/blob/main/prueba.md)
- [ ] Crear una nueva DB en postgreSQL, en caso de ser necesario usar el servicio **gratuito ** https://www.elephantsql.com/ u otro equivalente
- [ ] Cargar archivo [db.sql][https://gitlab.com/jua.retamales/sunai-etl-test/-/blob/main/db.sql] en caso de problemas cargar por partes
- [ ] Crear rama (branch)
- [ ] Hacer push
- [ ] Pedir merge (**sin embargo no aceptarlo y solo crear la petición**)
- [ ] Enviar un correo con los datos de acceso a PostgreSQL y url del repositorio indicando el termino del proyecto con asunto "Desafío Sunai 1.0".

Nota: 

1. Ante cualquier duda, problema e inquietud, enviar un correo a jretamales@sunai.cl con el asunto "Desafío Sunai 1.0", dentro indicar su nombre y describir el problema
2. En caso de alguna dificultad que impida la completitud del programa, desarrollar todo lo posible y en el correo comunicar los problemas en caso de no poder recibir respuestas a tiempo
3. Si tienes problema para usar git, enviar la solución adjunta al correo